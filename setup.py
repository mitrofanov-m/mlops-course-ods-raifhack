from setuptools import find_packages, setup

setup(
    name='raiflib',
    packages=find_packages(),
    version='0.0.1',
    description='MLops wrapper of competition solution',
    author='mitrofanov-m',
    license='MIT',
)
